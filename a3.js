/*
Fazer uma função que retorne um array com a soma dos valores em posições iguais de 2
arrays de n lados. 
ex: [[1,2],[2,6]] , [[3,4],[3,9]]]

    r = [[3,8], [6,13]]
    1+2 = 3
    2+6 = 8
    3+3 = 6
    4+9 = 13
*/ 

function somaValsPos(l1,l2){
    var nova = []

    v1 = l1[0][0] +  l1[1][0]
    v2 = l1[0][1] +  l1[1][1]
    v3 = l2[0][0] +  l2[1][0]
    v4 = l2[0][1] +  l2[1][1]

    a = [v1,v2] 
    b = [v3,v4]
    nova.push(a)
    nova.push(b)
    console.log(nova)
}


function somaArrays(a, b){

    let novoArray = [];
    for (i in a){
        let novaLin = [];
        let linA = a[i];
        let linB = b[i];

        for (j in linB){
            let elemA = linA[j];
            let elemB = linB[j];
            let soma = elemA + elemB
            novaLin.push(soma);
        }
        novoArray.push(novaLin)
    }
    console.log(novoArray)
};

var matriz1 = [[1,2],[2,6]] 
var matriz2 = [[3,4],[3,9]]
somaValsPos(matriz1,matriz2)
somaArrays(matriz1,matriz2)